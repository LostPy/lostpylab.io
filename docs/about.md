---
title: LostPy - About me
description: Ce site est le lieux où je partage mes projets.
hide:
  - navigation
  - toc
---

# About me

![lostpylogo](assets/logos/LostPyLogo.gif){ loading=lazy }

!!! abstract "En bref"

    J'ai 22 ans et j'ai terminé en 2023 ma formation dans une école d'ingénieur généraliste. Avec cette formation, j'ai des connaissances en génie industriel, en génie mécanique ainsi que des connaissances en électronique et en systèmes intégrés.

    Etant passionné par les problèmes de réflexions et les applications très diverses de la programmation, j'ai appris le langage de programmation Python en autodidacte après avoir vu les bases durant quelques cours dans le cadre de ma formation. Par la suite j'ai commencé à apprendre le langage Rust ainsi que le C dans le cadre d'un cours sur les microcontrôleurs.


## Compétences

### Langages de Programmation

 - [x] **Python**
 - [x] **Rust**
 - [x] C
 - [x] C++

#### Bibliothèques

=== "Python"

     - [x] Bibliothèque standard
     - [x] pandas
     - [x] numpy
     - [ ] scipy
     - [x] PyQt5 & PySide6 (Qt)
     - [x] scikit-learn
     - [x] aiohttp
     - [x] requests
     - [x] ...

=== "Rust"

     - [x] serde
     - [x] embedded-hal
     - [x] avr-hal
     - [x] pyo3
     - [x] bevy

=== "C"

     - [x] Arduino


=== "C++"

     - [x] Qt


??? tip

    Le logo que vous pouvez voir en haut de cette page à été réalisé à l'aide du package [Manim](https://docs.manim.community).  

    ??? note "Code"

        ```py title="scene.py"
        from manim import Scene, Write
        from manim import (
          Text
        )
        from manim import (
          WHITE,
          BLACK,
        )


        class LostPyLogo(Scene):

          def construct(self):
              self.camera.background_color = WHITE
              text = Text(
                  "LostPy",
                  t2c={'[0:4]': "#1864ab", '[4:]': "#f59f00"},
                  font_size=48
              )
              self.play(Write(text))
        ```
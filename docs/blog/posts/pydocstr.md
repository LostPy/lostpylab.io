---
title: pydocstr
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-04-25
categories:
  - Projet
  - Documentation
  - Python
authors:
  - lostpy
tags:
  - python
  - python package
  - library
  - docs
---

# pydocstr

pydocstr est un package Python pour générer les docstrings des fonctions et des classes d'un fichier ou d'un package Python.

[Repository :material-source-repository:](https://gitlab.com/lostpy/pydocstr){ .md-button }

<!-- more -->

## Autres packages requis

### Optionnel

 * [colorama](https://pypi.org/project/colorama/) : Pour avoir les logs en couleurs.
 * [yaml](https://pypi.org/project/PyYAML/) : Pour utiliser les fichiers de configuration dans le format YAML.

## Installation


=== "Version stable"

    ```bash
    pip install git+https://github.com/LostPy/pydocstr.git@stable
    ```

=== "Version beta"

    ```bash
    pip install git+https://github.com/LostPy/pydocstr.git@beta
    ```

## Utilisation

### Sur un module indépendant

 1. Indiquer dans les différents fichiers les fonctions et les classes à documenter, et optionnellement, préciser la description de la fonction/classe.

    ??? example
        ```py title="module_to_document.py"
        from pyDocStr import to_document


        @to_document(description='A function which print these arguments.')
        def function_one(arg1, arg2: int, arg3: str, arg4: float = 1.):
            print(arg1, arg2, arg3, arg4)


        @to_document(description='A function wich return a tuple.')
        def function_two() -> tuple:
            return (0, 0)
        ```

 2. Utiliser la commande suivante suivante :

    ```bash
    python -m pyDocStr ./module_to_document.py -o ./module_documented.py
    ```

### Sur un package

 1. Dans les différents fichiers du package, indiquer les différentes fonctions et classes à documenter comme iniquer dans la partie précédente "Sur un module indépendant".

 2. Utiliser la commande suivante :

    ```bash
    python -m pyDocStr -p path/of/package
    ```

!!! info

    Il est possible de choisir le formattage des docstrings en spécifiant l'argument `formatter` pour un formattage existant. Il peut être personnaliser en spécifiant un fichier de configuration via l'argument `config-formatter`.

    ```bash
    python -m pyDocStr -p path/of/package --formatter numpy --config-formatter path/config.json
    ```

??? info "Message d'aide"

    ```
    usage:  [-h] [-p [PACKAGE]] [--no-sub] [--decorator-name [DECORATOR_NAME]] [-o [OUTPUT]] [--formatter {simple,numpy}] [--config-formatter [CONFIG_FORMATTER]]
            [--level-logger {debug,info,warning,error}]
            [file]

    A package to generate a complete documentation in your python files.

    positional arguments:
      file                  path of python file to document.

    optional arguments:
      -h, --help            show this help message and exit
      -p [PACKAGE], --package [PACKAGE]
                            path of a package to document. If this argument is used, a script to document the package is created.
      --no-sub              If you wan't document subdirectories of directory passed to --directory argument or subpackage of package passed to --package argument.
      --decorator-name [DECORATOR_NAME]
                            The decorator name use for 'to_document' decorator.
      -o [OUTPUT], --output [OUTPUT]
                            The output path, if not specify, the files are overwrite. The output must be a folder if --directory argument is passed else a file.
      --formatter {simple,numpy}
                            The formatter to use if 'config' parameters is not specified.
      --config-formatter [CONFIG_FORMATTER]
                            path of a config file for formatter.
      --level-logger {debug,info,warning,error}
                            The logger level.
    ```

!!! warning

    Pour le moment, la documentation d'un package génère un fichier Python à exécuter pour documenter le package. Une mise à jour sera faite pour éviter cela et documenter un package en exécutant la commande n'importe où .

## Configuration

???+ info

    Le fichier de configuration peut être un fichier `json` ou un fichier `yaml`

    Pour le fichier `yaml` il faut installer le package pyyaml

    ```bash
    pip install pyyaml
    ```

Le fichier de configuration est un fichier contenant 6 mots clés qui ont pour valeurs une chaîne de caractères contenant d'autres mots clés utilisé pour construire les docstrings.

|nom|type|description|mots clés|
|:--:|:--:|-----------|--------|
|`description`|*str*|Le format de la description d'un docstring|`description`|
|`fields`|*str*|Le format d'une section dans un docstring (sections = `Parameters`, `Returns`...)|`name`, `items`, `prefix`, `suffix`|
|`items`|*str*|Le format d'un item (attribut, argument...), un item est un élément d'une section (parameter...)|`name`, `description`, `type`, `default`|
|`prefix`|*str*|Le préfixe du nom d'une section. Le préfixe est répété autant de fois que le nombre de caractères dans le nom de la section.||
|`suffix`|*str*|Le suffixe du nom d'une section. Le suffixe est répété autant de fois que le nombre de caractères dans le nom de la section.||


!!! warning

    **Les mots-clés (colonne "mots clés" du tableau) doivent être spécifiés entre `{}`** : `{keyword}`.

!!! info

    Les valeurs `null` des fichiers de configuration sont remplacées par les valeurs par défaut.

??? example

    === "JSON"

        ```json title="docstr_config.json"
        {
          "description": "{description}\n",
          "fields": "{prefix}\n{name}\n{suffix}\n{items}",
          "items": "{name} : {type}\n\t{description}\n\t{default}",
          "prefix": "",
          "suffix": "-"
        }
        ```

    === "YAML"

        ```YAML title="docstr_config.yml"
        # The format for the description of a function or a class. The key word '{description}' is mandatory.
        description: "{description}\n"

        # The format of a field in docstring ('Parameters', 'Returns'...).
        fields: "{prefix}\n{name}\n{suffix}\n{items}"

        # The format of a item in a field (a parameter...).
        items: "{name} : {type}\n\t{description}\n\t{default}"

        # The prefix use for fields. Use only if 'prefix' key word is use in 'fields'.
        # This prefix is repeated so that it has the same length as the field name.
        prefix: ''

        # The suffix use for fields. Use only if 'suffix' key word is use in fields.
        # This suffix is repeated so that it has the same length as the field name.
        suffix: '-'  # With the field 'Parameters', this prefix give '----------' (10*'-')
        ```

### Commande

Pour utiliser un fichier de configuration, il faut utiliser l'argument `config-formatter` :

```bash
python -m pyDocStr -p path/of/package --config-formatter path/config.json
```
---
title: Rust & Arduino - Bases
description: Article présentant l'utilisation de Rust pour programmer une carte Arduino (Uno).
draft: true
date: 2022-10-01
categories:
  - Article
  - Embedded
  - Arduino
  - Rust
authors:
  - lostpy
tags:
  - rust
  - arduino
  - embeded
---


# Rust & Arduino - Bases

Cet article présente les bases pour programmer une carte Arduino (Uno en l'occurence) avec le langage de programmation Rust.

<!-- more -->

## Configuration utilisée

Machine et système utilisé pour le développement :

 * linux 64 bit architecture ARM (Ubuntu 20)

Rust :

|Programme|Version|commit|date du commit|
|:-------:|:-----:|:----:|:------------:|
|rustc|1.64.0-nightly|`6dba4ed21`|2022-07-09|
|cargo|1.64.0-nightly|`c0bbd42ce`|2022-07-03|
|rustup|1.25.1|`bb60b1e89`|2022-07-12|

!!! note

    En utilisant la configuration donné par le template [`arduino-hal`][avr-hal] il ne devrait pas y avoir de problème de version de Rust.

## Initialisation

Pour installer la bonne version de Rust et les outils nécessaires, j'ai réalisé un article dédié.

[Rust & Arduino (Installation)](./rust-arduino-install.md){ .md-button .md-button--primary }

## Premier programme

Pour commencer voyons juste le code permettant de faire clignoter une LED, le `Hello World` des programmes embarqués.

Si le projet Rust a été créé à partir du template de [`arduino-hal`][avr-hal] (avec la commande `cargo generate`), le code présent dans `src/main.rs` devrait déjà faire clignoter une LED. 
J'ai remis ce code ci-dessous en enlevant les commentaires inutiles et en modifiant légèrement :

```rust title="src/main.rs" linenums="1"
#![no_std]
#![no_main]

use panic_halt as _;

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);

    let mut led = pins.d13.into_output();

    loop {
        if led.is_set_low() {
            led.set_high();
        } else {
            led.set_low();
        }
        arduino_hal::delay_ms(1000);
    }
}
```

!!! Explications

    === "Lignes 1 et 2"
        ```rust
        #![no_std]
        #![no_main]
        ```

        Ces deux lignes contiennent un appel aux macros `no_std` et `no_main`.

         * `no_std` indique à Rust que le programme n'utilise pas la bibliothèque standard. 
            En effet, pour la programmation système et embarqué, nous ne pouvons pas utiliser la bibliothèque standard.
            Nous ne pouvons donc pas utiliser des crates (librairies) comme `std::io` pour les entrées/sorties par exemple.
         * `no_main` indique à Rust qu'il n'y a pas la fonction `main` habituelle, qui est l'entrée du programme. 
           Il faudra donc ensuite spécifier la fonction d'entrée du programme.

    === "Lignes 4"
        ```rust
        use panic_halt as _;
        ```

        Cette ligne importe la crate (librarie) `panic_halt` qui initialise le comportement de panique.

    === "Lignes 6 et 7"
        ```rust
        #[arduino_hal::entry]
        fn main() -> ! {...}
        ```

        Ces lignes exécutent la macro `entry` de `arduino_hal` sur la fonction `main`. 
        Cette macro indique à Rust que cette fonction est l'entrée de notre programme.
        Le type de retour `!` indique à Rust que cette fonction ne se terminera jamais (présence d'une boucle infinie).

    === "Lignes 8 et 9"
        ```rust
        let dp = arduino_hal::Peripherals::take().unwrap();
        let pins = arduino_hal::pins!(dp);
        ```

        Ces lignes sont l'équivalent de ce que nous mettons dans la fonction `void setup()` d'Arduino (en C).

        La première ligne est une ligne courante dans l'écosystème Rust pour l'embarqué utilisant la crate `embeded_hal`.
        Elle récupère les différents périphériques disponibles (les registres des ports, ...). `dp` sera utilisé pour accéder aux pins notamment.

        C'est justement ce que nous faisons dans la deuxième ligne, `arduino_hal` propose la macros `pins` qui prend les périphériques en argument (ici `dp`) 
        et nous retourne un objet contenant tout les pins d'Arduino, les différents pins pourront être accessible par leur nom, par exemple `d5` pour le pin "D5" 
        ou `a0` pour le pin "A0".

    === "Ligne 11"
        ```rust
        let mut led = pins.d13.into_output();
        ```

        Cette ligne initialise le pin `D13` comme sortie et nous stockons ce pin dans la variable `led` que nous utiliserons pour faire clignoter la LED.

    === "Lignes 13"
        ```rust
        loop {...}
        ```

        L'instruction `loop` réalise une boucle infinie (qui peut être arrêter par un `break`).
        Elle contiendra le programme qui contrôlera la carte Arduino, c'est l'équivalent de la fonction `void loop()` en Arduino.

    === "Lignes 14 à 18"
        ```rust
        if led.is_set_low() {
            led.set_high();
        } else {
            led.set_low();
        }
        ```

        Ce code permet de changer l'état du pin connecté à la LED :

         * Si l'état est à l'état bas (0 ou ~0V), on le passe à l'état haut (1 ou ~3,3V).
         * Sinon on passe l'état à l'état bas (0 ou ~0V).

        Ce code est équivalent l'appel de la méthode `toggle` :
        ```rust
        led.toggle();
        ```

    === "Lignes 19"
        ```rust
        arduino_hal::delay_ms(1000);
        ```

        Cette ligne permet de faire une pause dans le programme de `1000 ms` soit 1 seconde.
        C'est l'équivalent de la fonction `delay(1000)` en Arduino.

## Structure d'un programme

A partir du [premier programme](#premier-programme), nous pouvons voir la structure d'un programme Rust pour Arduino comme suit :
```rust
#![no_std] // (1)!
#![no_main] // (2)!

// Importation des crates
use panic_halt as _; // (3)!
...

// Initialisation des constantes
...

#[arduino_hal::entry] // (4)!
fn main() -> ! {
    // Initialisation du programme
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp); // (5)!
    ...

    // Corps du programme (répété indéfiniment)
    loop {
        ...
    }
}
```

1. Indique que le programme n'utilise pas la bibliothèque standard.
2. Indique que le programme n'a pas la fonction d'entrée `main` habituelle.
3. Initialise le comportement des paniques.
4. Indique que la fonction `main` est l'entrée du programme.
5. Créer un objet contenant les pins de la carte Arduino.

!!! warning

    Cet article est encore en cour d'écriture.

[avr-hal]: https://github.com/Rahix/avr-hal/
[ravedude]: https://github.com/Rahix/avr-hal/tree/main/ravedude

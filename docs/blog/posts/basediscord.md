---
title: baseDiscord-py
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-06-22
categories:
  - Projet
  - Discord
  - Python
authors:
  - lostpy
tags:
  - python
  - python package
  - library
  - discord
---


# baseDiscord-py

[baseDiscord][repo] est un package Python pour avoir une base de bot [Discord][discord] en utilisant le package [`discord.py`][discordpy].

Ce package apporte une gestion de certaines erreurs (souvent par l'envoie d'un message d'erreur à l'utilisateur du bot) et par quelques commandes utilent comme une commande pour le owner du bot qui permet d'arrêter le bot et des commandes d'aide par défaut.

[Documentation :page_facing_up:](https://lostpy.gitbook.io/basediscord-py/){ .md-button .md-button--primary }  [Repository :material-source-repository:][repo]{ .md-button }

<!-- more -->

!!! warning

    Le package [`discord.py`][discordpy] n'est plus maintenu depuis l'été 2021. Il est préférable d'utiliser d'autres packages. Je ferais probablement un autre package apportant comme celui une base de bot pour un autre package discord.


## Installation

Ce package peut être installer avec `pip` et `git` :

=== "Dernière version"

    ```bash
    pip install git+https://github.com/LostPy/baseDiscord-py.git@master
    ```

=== "Beta"

    ```bash
    pip install git+https://github.com/LostPy/baseDiscord-py.git@beta
    ```

<div class="termy">

```console
$ pip install git+https://github.com/LostPy/baseDiscord-py.git@master

---> 100%
Successfully installed baseDiscord
```

</div>

## Utilisation

### Base

```py title="main.py"
from discord_slash import SlashCommand
from baseDiscord import BaseBot, Help, Owner



# Setting

TOKEN = "my token"  # A ne pas mettre dans le code en clair, il est mis ici pour l'exemple
COLOR = discord.Colour.green()
COLOR_ERROR = discord.Colour.red()
PERMISSIONS = 2147608640
PREFIX = 'test!'


# Init

bot = BaseBot(
    TOKEN,
    command_prefix=PREFIX,
    color=COLOR,
    color_error=COLOR_ERROR,
    permissions=PERMISSIONS,
    intents=discord.Intents.all()
)
slash = SlashCommand(bot, sync_commands=True)

Help.setup(bot)
Owner.setup(bot)

bot.run()
```

### Bot personnalisé


```py title="bot.py"
import discord
from baseDiscord import BaseBot


class MyBot(BaseBot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def messages_on_ready(self):
        self.logger.info("My custom message")
```

```py title="main.py"
from discord_slash import SlashCommand
from baseDiscord import Help, Owner

from bot import MyBot



# Setting

TOKEN = "my token"  # A ne pas mettre dans le code en clair, il est mis ici pour l'exemple
COLOR = discord.Colour.green()
COLOR_ERROR = discord.Colour.red()
PERMISSIONS = 2147608640
PREFIX = 'test!'


# Init

bot = MyBot(
    TOKEN,
    command_prefix=PREFIX,
    color=COLOR,
    color_error=COLOR_ERROR,
    permissions=PERMISSIONS,
    intents=discord.Intents.all()
)
slash = SlashCommand(bot, sync_commands=True)

Help.setup(bot)
Owner.setup(bot)

bot.run()
```

[repo]: https://gitlab.com/LostPy/baseDiscord-py
[discord]: https://discord.com/
[discordpy]: https://discordpy.readthedocs.io/en/latest/index.html
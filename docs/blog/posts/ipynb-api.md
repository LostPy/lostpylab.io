---
title: ipynb-api
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-09-30
categories:
  - Projet
  - Jupyter Notebook
  - Python
  - Rust
authors:
  - lostpy
tags:
  - rust
  - python package
  - rust crate
  - library
  - jupyter notebook
  - learning
---

# ipynb-api

!!! info

    Cette librairie a été créée pour m'entrainer à utiliser le langage Rust et créer des packages Python en Rust.

`ipynb-api` est une librairie pouvant être utilisée pour le langage Rust et pour Python. Cette librairie permet d'exporter un Jupyter Notebbok en un fichier Markdown.

[Repository :material-source-repository:](https://github.com/lostpy/ipynb-api){ .md-button }

<!-- more -->

## Installation

Si vous avez [Rust](https://www.rust-lang.org/) d'installé vous pouvez utiliser ce package en suivant les étapes suivantes :

 1. Cloner le répertoire :

    ??? quote "Commande"

        ```bash
        git clone https://github.com/LostPy/ipynb-api.git
        ```

 2. Installer [maturin](https://lib.rs/crates/maturin)

    ??? quote "Commande"

        ```bash
        pip install maturin
        ```

 3. Utiliser la commande `maturin develop`


## utilisation

!!! warning

    Ces étapes nécessitent d'avoir installé le répertoire avec la méthode décrite dans la section installation.

=== "Avec la CLI"
    
    !!! info
        Ces actions sont effectuées dans le dossier parent du répertoire `ipynb-api`

    Pour exporter un Notebook `.ipynb` en fichier Markdown `.md`, il faut utiliser la commande suivante :

    ```bash
    python ipynb-api mon_notebook.ipynb --output version_markdown.md
    ```

=== "Dans un script"

    Placer le répertoire installé précédamment dans le projet où vous voulez l'utiliser.

    Pour charger un fichier notebook et exporter le contenu en Markdown :

    ```py
    from ipynb_api import Notebook

    my_notebook = Notebook("mon_notebook.ipynb")

    markdown = my_notebook.to_markdown()
    ```

---
title: QtCustomWidgets
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-04-18
categories:
  - Projet
  - Qt
  - Python
authors:
  - lostpy
tags:
  - python
  - python package
  - library
  - qt
---

# QtCustomWidgets

Un package Python regroupant mes Widgets Qt créés pour différentes versions de Qt.

!!! info

    Depuis la sortie de Qt6, je réalise les applications Qt principalement à l'aide du package [PySide6](), les derniers widgets que je réalise seront principalement disponiblent pour ce package et cette version.

[Repository :material-source-repository:](https://gitlab.com/lostpy/qtcustomwidgets){ .md-button }

<!-- more -->

## Installation

Vous pouvez installer soit une version spécifique pour un package, soit une version spécifique à une version de Qt.

=== "master"

    Correspond à la branche `Qt6`

    ```
    pip install git+https://github.com/LostPy/QtCustomWidgets.git@master
    ```

=== "Qt6"

    Installe le package compatible avec PySide6 (PyQt6 n'est pas supporté par mon package)

    ```
    pip install git+https://github.com/LostPy/QtCustomWidgets.git@Qt6
    ```

=== "Qt5"

    Installe le package compatible avec PySide2 et PyQt5

    ```
    pip install git+https://github.com/LostPy/QtCustomWidgets.git@Qt5
    ```

=== "PySide6"

    Installe le package compatible avec PySide6

    ```
    pip install git+https://github.com/LostPy/QtCustomWidgets.git@PySide6
    ```

=== "PySide2"

    Installe le package compatible avec PySide2

    ```
    pip install git+https://github.com/LostPy/QtCustomWidgets.git@PySide2
    ```

=== "PyQt5"

    Installe le package compatible avec PyQt5

    ```
    pip install git+https://github.com/LostPy/QtCustomWidgets.git@PyQt5
    ```

!!! info inline

    La branche `master` correspond à la branche `Qt6`.

!!! info

    La branche `Qt6` est uniquement compatible avec PySide6.

## Utilisation

Pour les branches `Qt6` et `Qt5` il faut importer le sous package correspondant au package que vous utilisez (avec X = 5 ou 6 selon la version) :

```py
from QtXCustomWidgets.<subpackage> import nameOfWidget
```

Pour les autres branches, il suffit d'importer le package et les widgets souhaités (avec X = 5 ou 6 selon la version) :

```py
from QtXCustomWidgets import nameOfWidget
```

!!! example

    === "Qt6"

        ```py
        from Qt6CustomWidgets.PySide6 import Progressbar
        ```

    === "Qt5"

        Si vous utilisez le package `PySide2` :

        ```py
        from Qt5CustomWidgets.PySide2 import Progressbar
        ```

        Si vous utilisez le package `PyQt5` :

        ```py
        from Qt5CustomWidgets.PyQt5 import Progressbar
        ```

    === "PySide6"

        ```py
        from Qt6CustomWidgets import Progressbar
        ```

    === "PySide2"

        ```py
        from Qt5CustomWidgets import Progressbar
        ```

    === "PyQt5"

        ```py
        from Qt5CustomWidgets import Progressbar
        ```


## Liste des Widgets

|Catégorie|Nom|Première version|Fonctionnel|QtDesigner|
|---------|---|:--------------:|:---------:|:--------:|
|Buttons|ToggleButtonAnimated|1.0.20210418|✅|✅|
|Graphics|GraphicWidget|1.0.20210418|✅|❌|
|Display|ProgressBar|1.0.20210418|✅|✅|
|Display|CircularProgressBar|1.0.20210425|❌|❌|
|Display|PlainTextEditHandler|1.0.20210429|✅|❌|
|Dialog|dialogLogger|1.0.20210429|✅|❌|
|Input|RangeSlider|1.1.20211101|✅|❌|

## Liste des Layout
|Nom|Première version|Fonctionnel|QtDesigner|
|---|:--------------:|:---------:|:--------:|
|FlowLayout|1.1.2|✅|❌|


!!! warning

    Les derniers Widgets et Layout que j'ai fais sont disponibles uniquement sur les versions pour Qt6 (PySide6).

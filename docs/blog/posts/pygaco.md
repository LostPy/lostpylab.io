---
title: PyGAco
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2022-08-07
categories:
  - Projet
  - Simulation
  - Python
authors:
  - lostpy
tags:
  - python
  - science
  - simulation
---


# PyGAco

[PyGAco][gitlab] est la première simulation avec visuel que j'ai réalisée. Il s'agit d'une simulation d'une colonie de fourmies.  
Il y a 3 versions offrant une variante de l'algorithme dû aux ajout que j'ai fais afin de l'améliorer.

[Repository :material-source-repository:][gitlab]{ .md-button }

<!-- more -->

## Utilisation

### Requirements

 * Python 3.10
 * arcade

Pour l'utiliser, il est préférable de créé un environnement virtuel sous Python 3.10.

??? note "Instructions"

    1. Cloner le repository :

    ```
    git clone https://gitlab.com/LostPy/pygaco.git
    ```

    ```
    cd pygaco/
    ```

    2. Créer et un environnement virtuel sous Python3.10 (et un système Linux) :

    ```
    python3.10 -m venv .venv/ && source .venv/bin/activate
    ```

    3. Installer les dépendances:

    ```
    pip install -r requirements.txt
    ```

Pour lancer une simulation :

```
python src/pygaco.py
```

Il est possible de modifier les paramètres dans `settings.yml`.


## Version 0.1.X

Dans cette version, une fourmie cherche la nourriture, dans l'ordre des priorités :

 1. Si elle a dans leur champ de vision de la nourriture, elle prend cette nourriture pour cible jusqu'à l'atteindre.
 2. Si elle a dans le champ de vision des phéromones rouges (déposées régulièrement quand elles transportent de la nourriture), elle se dirrige vers la phéromone la plus proche.
 3. S'il n'y a rien dans son champ de vision, elle se déplace de manière aléatoire.

Une fois qu'elle atteind une nourriture, elle la prend, et retourne à la colonie, dans l'ordre des priorités :

 1. Si elle est près de la colonie, elle se dirrige vers la colonie jusqu'à l'atteindre.
 2. Si elle a dans le champ de vision des phéromones bleues (déposées régulièrement quand elles ne transportent pas de nourriture), elle se dirrige vers la phéromone la plus proche.
 3. S'il n'y a rien dans son champ de vision, elle se déplace de manière aléatoire.

Les phéromones sont "détruitent" (disparraissent) après un certain temps.


## Version 0.2.X

Dans cette version, les phéromones ont un attribut `intensity`, quand une phéromone est créée, cet attribut et initialisé selon la distance qui le sépare de la colonie pour les phéromone bleues, et de la nourriture récoltée pour les phéromones rouges, plus la distance est grande, plus l'intensité est faible.

Les phéromones perdent en intensité avec le temps, quand l'intensité d'une phéromone atteint 0, cette phéromone est "détruite".

Les fourmies suivent les phéromones de plus forte intensité qu'elles voient (quand elles cherchent des phéromones).


## Version 0.3.X

Dans cette version, les fourmies ont une horloge interne (attribut `internal_clock`), cette horloge est réinitialisé à 0 quand elle ramène de la nourriture à la colonie où qu'elle trouve de la nourritue.

L'intensité des phéromones à la création dépend de cette horloge interne, plus la valeur est grande (plus la fourmie à voyagée) plus l'intensité sera faible.


[gitlab]: https://gitlab.com/LostPy/pygaco


---
title: Mkdocs & Material for Mkdocs
description: Article présentant Mkdocs & Material for Mkdocs ainsi que quelques plugins et extensions Mkdocs.
date: 2022-01-18
date_updated: 2022-10-01
categories:
  - Article
  - Web
authors:
  - lostpy
tags:
  - docs
---

# Mkdocs & Material for Mkdocs

???+ question "Qu'est-ce que c'est ?"

    [Mkdocs][mkdocs] est un package Python qui permet de simplifier la création de site web tout en étant très extensible et personnalisable, il est utilisé principalement pour créer des documentations. Cet outil permet de créer un site web en utilisant des fichiers [Markdown][markdown], un "markup language" (langage à balisage) très légé.

    [Material for Mkdocs][material] est un thème pour [Mkdocs][mkdocs] très populaire qui apporte aussi des extensions [Mkdocs][mkdocs] utiles. La plupart de ces extensions proviennent du plugin [Pymdown Extensions][pymdown], et apportent des fonctionnalités comme l'intégration du langage [LaTeX][latex] pour l'écriture de formule mathématique, la création de graphes avec [mermaid.js][mermaid]...

Cet article présente rapidement comment commencer un site avec [Mkdocs][mkdocs] et le thème [Material][material], ainsi que l'utilisation de quelques librairies courantes. Vous pourrez trouver une [liste de plugins](#plugins-utiles) pouvant être utiles selon votre utilisation à la fin de cet article.

<!-- more -->

??? tip

    Ce site est réalisé avec [Mkdocs][mkdocs] et [Material for Mkdocs][material] :smiley:

## Installation

!!! warning

    Python3 avec une version récente (3.7+) est requis!

[Mkdocs][mkdocs] et [Material for Mkdocs][material] sont des packages Python et peuvent donc s'installer avec `pip`.

=== "Mkdocs"
    ```bash
    pip install mkdocs
    ```

=== "Mkdocs for Material"

    ```bash
    pip install mkdocs-material
    ```

## Créer et initialiser un projet

Pour créer un nouveau site, il suffit d'utiliser une commande du programme [Mkdocs][mkdocs] :

```bash
mkdocs new path/directory
```
    
=== "Dossier actuel"

    ```bash
    mkdocs new .
    ```

=== "Autre dossier"

    ```bash
    mkdocs new ./my-project
    ```

Cette commande créée un répertoire `docs`, contenant un fichier `index.md`, et un fichier `mkdocs.yml`, votre répertoire devrait avoir cette structure (sans les autres fichiers et répertoires) :

```
.
├── docs
│   └── index.md
└── mkdocs.yml
```

### Fichier Requirements

Je conseille d'ajouter un fichier `requirements.txt` dans lequel seront indiqués les packages Python utilisés pour le site, il est particulièrement utile pour le déploiment et si vous souhaitez proposer aux utilisateurs d'accéder à la doc en "hors ligne".

Il peut est être mis à la racine du projet ou bien dans le dossier `docs` :

```plaintext title="docs/requirements.txt"
mkdodcs
mkdocs-material
```

!!! info

    Pour plus d'information sur ce fichier, je conseille [cet article](https://learnpython.com/blog/python-requirements-file/).

    Pour installer les packages nécessaires au site, il suffira d'utiliser la commande :
    
    ```bash
    pip install -r docs/requirements.txt
    ```

    !!! warning
    
        **Il est conseillé d'ajouter les versions minimales aux packages**

### Structure d'un projet Mkdocs

Un site réalisé avec [Mkdocs][mkdocs] contient toujours un répertoire contenant l'ensemble des pages constituant le site ainsi que les ressources nécessaires, et un fichier de configuration `mkdocs.yml`.

Le fichier `index.md`, est l'entrée du site, c'est à cette page qu'on arrive.

### Fichier `mkdocs.yml`

Le fichier `mkdocs.yml` est le fichier de configuration de [Mkdocs][mkdocs]. Voici la liste des principaux champs souvent utilisés :

|Keyword|Type|Description|Nécessaire|
|:-----:|:--:|:----------|:--------:|
|site_name|String|Le nom du site|:material-checkbox-marked-outline:|
|site_url|String|L'URL du site|:material-checkbox-blank-outline:|
|repo_url|String|L'URL du repository où est stocké le site (repo GitHub, GitLab ou autre)|:material-checkbox-blank-outline:|
|site_description|String|Une courte description du site|:material-checkbox-blank-outline:|
|markdown_extensions|List|La liste des extensions markdown à utiliser|:material-checkbox-blank-outline:|
|plugins|List|La liste des plugins à utiliser|:material-checkbox-blank-outline:|
|theme|Dict|Le thème à utiliser|:material-checkbox-blank-outline:|
|extra_css|List|Liste des feuilles de styles à appliquer au site|:material-checkbox-blank-outline:|
|extra_javascript|List|Liste des scripts JavaScript à exécuter|:material-checkbox-blank-outline:|
|nav|Dict|Les différentes sections et page, les clés sont les noms des sections et pages à afficher et les valeurs leur emplacement dans le répertoire `docs`|:material-checkbox-blank-outline:|

!!! info

    La liste complète des champs avec leur description est accessible sur la page [configuration de Mkdocs](https://www.mkdocs.org/user-guide/configuration/).

Pour commencer, on ajoute les informations générales du site en ajoutant les champs `site_name`, `site_url`, optionnellement `repo_url` et `site_description`. Enfin peut ajouter un thème (ici material).

??? example
    Cas de ce site
    ```YAML title="mkdocs.yml"
    site_name: LostPy
    site_url: https://lostpy.gitlab.io

    repo_url: https://gitlab.com/lostpy/lostpy.gitlab.io
    edit_uri: ''
    site_description: LostPy's web site
    theme:
      name: material
      language: en
      palette:
        - scheme: slate
          primary: teal
          accent: teal
          toggle:
            icon: material/weather-sunny
            name: Switch to light mode
        - scheme: default
          primary: teal
          accent: teal
          toggle:
            icon: material/weather-night
            name: Switch to dark mode
      features:
        - navigation.tabs
        - navigation.sections
        - content.tabs.link
        - navigation.top
          logo: assets/logos/LostPyLogo.svg
          favicon: assets/logos/LostPyLogo.svg
    ```

    !!! info

        Mettre une chaîne de caractère vide pour `edit_uri` permet d'enlever le bouton pour éditer le contenu.

    !!! info "Thème"

        Pour plus d'information sur la configuration du thème, voir sur [Material for Mkdocs][material]

        [Configuration](https://squidfunk.github.io/mkdocs-material/creating-your-site#configuration){ .md-button .md-button--primary }


!!! success

    Maintenant que nous avons créé et initialisé notre site, on peut commencer à ajouter des pages.

    On peut également tester cette base en exécutant dans le dossier du site la commande :

    ```bash
    mkdocs serve
    ```


## Utilisation

### Ajouter des pages

Chaque page sera un fichier, il suffit que de créer des fichiers dans le dossier `docs` pour en ajouter. Pour organiser le répertoire vous pouvez également créer un dossier pour chaque section dans lequel vous intégrez vos pages pour cette section.

Ensuite il faut ajouter ces pages dans la section `nav` du fichier `mkdocs.yml`.

!!! important

    **Tout les emplacements sont indiqués en relatif par rapport au dossier `docs`**, par exemple le fichier `index.md` est à la racine du dossier `docs`, on indiquera son emplacement simplement avec `index.md`.

??? example

    === "Complet"

        === "`mkdocs.yml`"

            ```yaml
            nav:
              - Home: index.md
              - Section 1:  # Section sans page d'accueil
                - Partie A: section1/partieA.md
                - Partie B: section1/partieB.md
              - Section 2:
                - section2/index.md  # Page "d'accueil" de la section 2
                - Partie C: section2/partieC.md
                - Partie D: section2/partieD.md
              - About: about.md
            ```

        === "Dossier"

            ```
            .
            ├── docs
            │   ├── about.md
            │   ├── index.md
            │   ├── section1
            │   │   ├── partieA.md
            │   │   └── partieB.md
            │   └── section2
            │       ├── index.md
            │       ├── partieC.md
            │       └── partieD.md
            └── mkdocs.yml
            ```

    === "Section 1"

        Une section est un mot clé avec une liste d'items (clé/valeur) :

        === "`mkdocs.yml`"
            
            ```yaml
            nav:
              # ...
              - Section 1:  # Section sans page d'accueil
                - Partie A: section1/partieA.md
                - Partie B: section1/partieB.md
              # ...
            ```

        === "Dossier"

            ```
            section1
            ├── partieA.md
            └── partieB.md
            ```

    === "Section 2"

        Une section peut contenir une page d'accueil en mettant l'emplacement du fichire sans clé :

        === "`mkdocs.yml`"

            ```yaml
            nav:
              # ...
              - Section 2:
                - section2/index.md  # Page "d'accueil" de la section 2
                - Partie C: section2/partieC.md
                - Partie D: section2/partieD.md
              # ...
            ```

        === "Dossier"

            ```
            section2
            ├── index.md
            ├── partieC.md
            └── partieD.md
            ```

### Editer une page

La plupart des fichiers utilisés sont des fichiers Markdown, pour voir en détail la syntaxe Markdown, je conseille de regarder le [site officiel](https://daringfireball.net/projects/markdown/) et/ou [ce tutoriel](https://www.markdownguide.org/basic-syntax/).

!!! note

    Avec [Mkdocs][mkdocs], il y a quelques spécificités qu'on retrouve dans le [guide d'utilisateur](https://www.mkdocs.org/user-guide/writing-your-docs/#writing-with-markdown).

??? example
    
    === "Markdown"

        ```Markdown

        Voici un paragraphe normal avec du texte en **gras** ou encore en *italique*. On peut mettre un lien vers le site de [Mkdocs](https://www.mkdocs.org/) ou bien vers la [section license](license).

        Ci-dessous un bloc de code :

        ```python
        def function(a: int, b: int):
            print(a, b)
            return a + b
        ``` <!-- ceci est un commentaire -->

        Un bloc de code avec un titre :

        ```c title="main.c"
        int function(int a, int b) {
            return a + b;
        }
        ``` <!-- Les retours à la ligne sont importants autour du bloc de code et après le langage ou le titre -->

        ## License

        Voici la section license...
        ```

    === "Résultat"

        Voici un paragraphe normal avec du texte en **gras** ou encore en *italique*. On peut mettre un lien vers le site de [Mkdocs](https://www.mkdocs.org/) ou bien vers la [section license](#license).

        Ci-dessous un bloc de code :

        ```python
        def function(a: int, b: int):
            print(a, b)
            return a + b
        ```
        <!-- ceci est un commentaire-->

        Un bloc de code avec un titre :

        ```c title="main.c"
        int function(int a, int b) {
            return a + b;
        }
        ```

        ## License

        Voici la section license...


!!! success

    Maintenant que le site est créé, on peut le tester localement en exécutant la commande :

    ```bash
    mkdocs serve
    ```

## Quelques Plugins

Voici une liste de plugins pouvant être utiles, pour chaque plugin j'ai mis une courte description puis la configuration à ajouter au fichier `mkdocs.yml` et un exemple d'utilisation avec le résultat.

### Details (& Admonitions)

Cette extension permet d'ajouter des blocs mettant en évidence une information. C'est tout ces blocs colorés que vous pouvez voir sur cette page ("Info", "Example", "Warning"...).

!!! example "Exemple Details"

    === "Configuration"

        ```YAML title="mkdocs.yml"
        markdown_extensions:
          - admonition
          - pymdownx.details
          - pymdownx.superfences  # permet l'imbrication de ces éléments
        ```

    === "Markdown"

        ```Markdown
        !!! Note

            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

        ??? quote "Titre modifié"

            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

        ???+ warning

            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

            ??? info

                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.
        ```

    === "Résultat"

        !!! Note

            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

        ??? quote "Titre modifié"

            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

        ???+ warning

            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

            ??? bug

                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

[Référence Material](https://squidfunk.github.io/mkdocs-material/reference/admonitions/){ .md-button .md-button--primary }
[Référence Pymdown](https://facelessuser.github.io/pymdown-extensions/extensions/details/){ .md-button .md-button--primary }

### Tabs

Cette extension permet de mettre du contenu dans des tabs, permettant ainsi d'alléger le site visuellement et de le rendre un peu plus intéractif. Sur cette page, vous venez d'en voir un juste au dessus présentant la configuration, la syntaxe et le résultat du plugin `details`.

!!! example "Exemple Tabs"

    === "Configuration"

        ```YAML title="mkdocs.yml"
        markdown_extensions:
          - pymdownx.superfences
          - pymdownx.tabbed
        ```

    === "Markdown"

        ```Markdown
        === "Tab 1"

            Un paragraphe.

            Un autre paragraphe suivis d'une information :

            !!! info

                Ceci est une information.

        === "Tab 2"

            Contenu du deuxième tab.
        ```

    === "Résultat"

        === "Tab 1"

            Un paragraphe.

            Un autre paragraphe suivis d'une information :

            !!! info

                Ceci est une information.

        === "Tab 2"

            Contenu du deuxième tab.

[Référence Material](https://squidfunk.github.io/mkdocs-material/reference/content-tabs/){ .md-button .md-button--primary }
[Référence Pymdown](https://facelessuser.github.io/pymdown-extensions/extensions/tabbed/){ .md-button .md-button--primary }


### mermaid (Diagram)

Ce plugin permet d'ajouter des graphes avec [mermaid.js][mermaid].

!!! example "Exemple mermaid"

    === "Configuration"

        ```YAML title="mkdocs.yml"
        markdown_extensions:
          - pymdownx.superfences:
              custom_fences:
                - name: mermaid
                  class: mermaid
                  format: !!python/name:pymdownx.superfences.fence_code_format
        ```

    === "Markdown"

        ```Markdown

        Flow chart

        ```mermaid
        graph TD;
            A-->B;
            A-->C;
            B-->D;
            C-->D;
        ``` \

        Pie chart

        ```mermaid
        pie
            title Key elements in Product X
            "Calcium" : 42.96
            "Potassium" : 50.05
            "Magnesium" : 10.01
            "Iron" :  5
        ``` \

        ```

    === "Résultat"

        Flow chart

        ```mermaid
        graph TD;
            A-->B;
            A-->C;
            B-->D;
            C-->D;
        ```

        Pie chart

        ```mermaid
        pie
            title Key elements in Product X
            "Calcium" : 42.96
            "Potassium" : 50.05
            "Magnesium" : 10.01
            "Iron" :  5
        ```

[Référence Material](https://squidfunk.github.io/mkdocs-material/reference/diagrams/){ .md-button .md-button--primary }
[Référence mermaid.js][mermaid]{ .md-button .md-button--primary }


### Autre plugins

Vous pouvez trouver d'autres plugins utiles sur le site de [Material for Mkdocs](https://squidfunk.github.io/mkdocs-material/reference/).

Voici une liste de plugins que je trouve intéressant :

 * [Annotations](https://squidfunk.github.io/mkdocs-material/reference/annotations/)
 * [Buttons](https://squidfunk.github.io/mkdocs-material/reference/buttons/)
 * [Icons + Emojis](https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/)
 * [Images](https://squidfunk.github.io/mkdocs-material/reference/images/)
 * [Lists](https://squidfunk.github.io/mkdocs-material/reference/lists/)
 * [MathJax](https://squidfunk.github.io/mkdocs-material/reference/mathjax/)

## Déployer le site

!!! warning

    Le déploiment peut varier selon la solution d'hébergement que vous utilisez. J'expliquerai ici la méthode pour **GitLab Page** que j'ai déjà utilisée et la méthode pour **GitHub Page**.

!!! info

    Vous pouvez retrouver ces informations sur le site [Material for Mkdocs](https://squidfunk.github.io/mkdocs-material/publishing-your-site/).

### GitHub Page

=== "Avec Mkdocs"

    [Mkdocs][mkdocs] permet de déployer sur GitHub Page manuellement en utilisant la commande :

    ```bash
    mkdocs gh-deploy --force
    ```

=== "Avec GitHub Action"

    Pour automatiser le déploiement, il est possible d'utiliser GitHub Action.

    Pour cela, il est nécessaire de créer le dossier `.github/workflows` et d'y ajouter le fichier suivant :

    ```YAML title=".github/workflows/ci.yml"
    name: ci 
    on:
      push:
        branches:
          - master 
          - main
    jobs:
      deploy:
        runs-on: ubuntu-latest
        steps:
          - uses: actions/checkout@v2
          - uses: actions/setup-python@v2
            with:
              python-version: 3.x
          - run: pip install -r docs/requirements.txt 
          - run: mkdocs gh-deploy --force
    ```

### GitLab Page

Avec GitLab Page, il faut utiliser [GitLab CI](https://docs.gitlab.com/ee/ci/), en ajoutant un fichier `.gitlab-ci.yml` dans la racine du projet :

```YAML title=".gitlab-ci.yml"
image: python:3.9-buster

before_script:
  - pip install -r docs/requirements.txt

test:
  stage: test
  script:
  - mkdocs build --strict --verbose --site-dir test
  artifacts:
    paths:
    - test
  except:
  - master

pages:
  stage: deploy
  script:
  - mkdocs build --strict --verbose --site-dir public
  artifacts:
    paths:
    - public
  only:
  - master
```

!!! info

    Si besoin, il est possible de rajouter des scripts à éxécuter pour installer les dépendances nécessaires. Si ces dépendances sont des packages Python, elles peuvent être indiqués dans le fichier `docs/requirements.txt`.


## Plugins utiles

|Plugin|Catégorie|Description|
|:----:|:-------:|:----------|
|[**Pymdown Extensions**][pymdown]|Tout usage|Une collection d'extensions utiles pour tout type de site|
|[**mkdocstring**](https://mkdocstrings.github.io/)|Documentation|Permet de générer automatiquement une documentation d'un programme / package Python à partir des `docstring`|
|[**mkdocs-jupyter**](https://github.com/danielfrg/mkdocs-jupyter)|Data & Python|Permet l'intégration de Jupyter Notebook et de fichier Python|
|[**mkdocs-table-reader**](https://github.com/timvink/mkdocs-table-reader-plugin)|Data|Permet d'ajouter des tableaux de données à partir de fichier CSV, Excel... Ce plugin utilise la package Python [`pandas`](https://pandas.pydata.org/).|


[mkdocs]: https://www.mkdocs.org/
[material]: https://squidfunk.github.io/mkdocs-material/
[pymdown]: https://facelessuser.github.io/pymdown-extensions/
[markdown]: https://daringfireball.net/projects/markdown/
[latex]: https://fr.wikibooks.org/wiki/LaTeX/%C3%89crire_des_math%C3%A9matiques
[mermaid]: https://mermaid-js.github.io/mermaid/

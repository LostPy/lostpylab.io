---
title: osupy
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-10-26
categories:
  - Projet
  - Game
  - Python
authors:
  - lostpy
tags:
  - python
  - python package
  - library
  - osu
  - osupy
---


# osupy

[osupy][site] est un package Python pour récupérer les données des beatmaps du jeu [osu!][osu], et les exporter dans différents formats. A l'avenir, il devrait également permettre la création de beatmap.

[Site :material-web:](https://osupy.gitlab.io/osupy/){ .md-button .md-button--primary } [Repository :material-source-repository:](https://gitlab.com/osupy/osupy){ .md-button }

<!-- more -->

## Installation

???+ tip

    Je recommande d'utiliser un [environnement virtuel](https://docs.python.org/3/tutorial/venv.html).
    Par exemple, dans le dossier de votre choix :

    === "Windows"

        ```bash
        python -m venv osu-env
        ```

    === "Linux (default Python version)"

        ```bash
        python3 -m venv osu-env
        ```

    === "Linux (specific Python version)"
        Replace `9` with the desired Python 3 version

        ```bash
        python3.9 -m venv osu-env
        ```

Pour installer ce package, il faut utiliser la commande suivante :

=== "master"

    === "Windows ou environnement"

        ```bash
        pip install git+https://gitlab.com/osupy/osupy.git
        ```

    === "Linux"

        ```bash
        pip3 install git+https://gitlab.com/osupy/osupy.git
        ```

=== "beta"

    === "Windows ou environnement"

        ```bash
        pip install git+https://gitlab.com/osupy/osupy.git@beta
        ```

    === "Linux"

        ```bash
        pip3 install git+https://gitlab.com/osupy/osupy.git@beta
        ```

=== "dev"

    === "Windows ou environnement"

        ```bash
        pip install git+https://gitlab.com/osupy/osupy.git@dev
        ```

    === "Linux"

        ```bash
        pip3 install git+https://gitlab.com/osupy/osupy.git@dev
        ```
 
<div class="termy">

```console
$ pip install git+https://gitlab.com/osupy/osupy.git

---> 100%
Successfully installed osupy
```

</div>

## Autres packages requis

 * [peace-performance-python][ppp]: Le package utilisé pour récupérer les données des fichiers de beatmap (.osu).

### Optionnel

 * [pandas][pd]: Pour exporter les données en `pandas.DataFrame`, dans un fichier csv ou dans un fichier Excel.
 * [pydub][pydub]: Pour utiliser les mp3
 * [scipy][scipy]: Pour exporter les données des fichiers mp3


## Utilisation en ligne de commande


La commande pour utiliser ce programme est :

```bash
python -m osupy
```


<div class="termy">

```console
$ python -m osupy

usage: osupy [-h] [--limit LIMIT] [--beatmap] [--set] [--output {csv,xlsx}] [--play] [path]

Utility to export beatmaps from osu! folder into xlsx or csv file.

positional arguments:
  path                 The path of beatmap, beatmap set or osu! folder

optional arguments:
  -h, --help           show this help message and exit
  --limit LIMIT        The maximum of beatmap to export. Default: no limit
  --beatmap            If "path" is a beatmap. Display data of the beatmap.
  --set                If "path" is a beatmap set. Display data of the beatmap set.
  --output {csv,xlsx}  If path is the osu! folder, indicate the output format of data.
  --play               If path is a beatmap or beatmap set, play the music of beatmap set.
```

</div>


=== "Export in CSV"

    Pour exporter les données de toutes les beatmaps dans un fichier csv, vous pouvez utiliser :

    ```bash
    python -m osupy path/of/osu --output csv
    ```

=== "Export in XLSX"

    Pour exporter les données de toutes les beatmaps dans un fichier xlsx, vous pouvez utiliser :

    ```bash
    python -m osupy path/of/osu --output xlsx
    ```

=== "Get Beatmap data"

    Pour voir les données d'un beatmap particulière, vous pouvez utiliser:
    ```bash
    python -m osupy path/of/osu/Songs/beatmapset/beatmap.osu --beatmap
    ```

=== "Get Beatmap set data"

    Pour voir les données d'un beatmap set particulier, vous pouvez utiliser :
    ```bash
    python -m osupy path/of/osu/Songs/beatmapset --set
    ```

Vous pouvez limiter le nombre de beatmap sets exportés en ajoutant l'argument `--limit`:

=== "Export in CSV"

    ```bash
    python -m osupy path/of/osu --output csv --limit 100
    ```

=== "Export in XLSX"

    ```bash
    python -m osupy path/of/osu --output xlsx --limit 100
    ```

[site]: https://osupy.gitlab.io/osupy
[osu]: https://osu.ppy.sh/home
[ppp]: https://github.com/Pure-Peace/peace-performance-python
[pd]: https://pandas.pydata.org/
[pydub]: https://github.com/jiaaro/pydub
[scipy]: https://docs.scipy.org/doc/scipy/index.html
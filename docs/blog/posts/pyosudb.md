---
title: pyosudb
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-10-28
categories:
  - Projet
  - Game
  - Python
authors:
  - lostpy
tags:
  - python
  - rust
  - python package
  - library
  - osu
  - osupy
---


# pyosudb

pyosudb est un package Python pour lire les fichiers `.db` du jeu [osu!][osu].

[Repository :material-source-repository:](https://gitlab.com/osupy/pyosudb){ .md-button }

<!-- more -->

## Installation

=== "Last version"

    ```bash
    pip install git+https://gitlab.com/osupy/pyosudb.git
    ```

=== "Beta version"

    ```bash
    pip install git+https://gitlab.com/osupy/pyosudb.git@beta
    ```

## Autres packages requis

Aucun :smiley:

## Utilisation

```py
import pyosudb

collection_path = "osu!/collection.db"

collections = pyosudb.OsuCollection(collection_path)

print(len(collections))
print(collections.names)
```

[osu]: https://osu.ppy.sh/home

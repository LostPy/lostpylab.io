---
title: vlcMusicPlayer
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-08-08
categories:
  - Projet
  - Python
authors:
  - lostpy
tags:
  - python
  - python package
  - library
  - learning
---

# vlcMusicPlayer


vlcMusicPlayer est un petit package Python pour créer un simple lecteur de musique avec un file d'attente. Ce package utilise le package [vlc](https://pypi.org/project/python-vlc/)

[Repository :material-source-repository:](https://gitlab.com/lostpy/vlcMusicPlayer){ .md-button }

<!-- more -->

## Autres packages requis

 * [python-vlc](https://pypi.org/project/python-vlc/)

## Installation

=== "Dernière version"

    ```bash
    pip3 install git+https://github.com/LostPy/vlcMusicPlayer.git@main
    ```

=== "Version beta"

    ```bash
    pip3 install git+https://github.com/LostPy/vlcMusicPlayer.git@beta
    ```

??? example

    ```py
    from vlcMusicPlayer import Music, MusicPlayer

    musics_path = ["music1.mp3", "music2.mp3"]

    player = MusicPlayer([Music(path) for path in musics_path])
    player.play()
    player.add_music("music4.mp3")
    ```


---
title: QPySimu
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2022-01-29
categories:
  - Projet
  - Simulation
  - GUI
  - Qt
  - Python
authors:
  - lostpy
tags:
  - python
  - qt
  - gui
  - osu
  - osupy
---

# QPySimu

Un package Python pour réaliser rapidement des prototypes de simulation à l'aide de Qt.

<!-- more -->


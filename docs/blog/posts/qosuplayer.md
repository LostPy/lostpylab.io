---
title: QOsuPlayer
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-11-03
categories:
  - Projet
  - Game
  - GUI
  - Qt
  - Python
authors:
  - lostpy
tags:
  - python
  - qt
  - gui
  - osu
  - osupy
---

# QOsuPlayer

QOsuPlayer est principalement un lecteur de musique dédié au jeu [osu!](https://osu.ppy.sh). Une fois l'emplacement du jeu donné, il récupère les musiques de toutes les beatmaps installées avec le jeu. Il peut également lire d'autre dossier contenant des fichiers mp3.

En plus de lire les musiques, QOsuPlayer peut afficher des statistiques sur un ensemble de beatmaps.

[Repository :material-source-repository:](https://gitlab.com/osupy/qosuplayer){ .md-button }

<!-- more -->

!!! warning

    Ce logiciel est en cour de développement.

## Installation

### A partir du code source

=== "Last version"

    Cloner le répertoire :

    ```bash
    git clone https://gitlab.com/osupy/qosuplayer.git
    ```

=== "Beta version"

    Cloner le répertoire :

    ```bash
    git clone https://gitlab.com/osupy/qosuplayer.git@beta
    ```

Puis éxécuter l'application avec :

```bash
python src/main.py
```

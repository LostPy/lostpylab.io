---
title: Rust & Arduino - Installation
description: Article présentant la meilleure solution que j'ai pu trouver pour créer des projets Arduino avec le langage Rust.
date: 2021-12-24
date_updated: 2022-10-01
categories:
  - Article
  - Embedded
  - Arduino
  - Rust
authors:
  - lostpy
tags:
  - rust
  - arduino
  - embedded
---

# Rust & Arduino - Installation

Cet article présente comment utiliser le langage Rust pour programmer une carte Arduino (Uno en l'occurence).

<!-- more -->

## Configuration utilisée

Machine et système utilisé pour le développement :

 * linux 64 bit architecture ARM (Ubuntu 20)

Rust :

|Programme|Version|commit|date du commit|
|:-------:|:-----:|:----:|:------------:|
|rustc|1.51.0-nightly|`e22670468`|2020-12-30|
|cargo|1.50.0-nightly|`75d5d8cff`|2020-12-22|
|rustup|1.24.3|`ce5817a94`|2021-05-31|

[Obtenir Rust](https://www.rust-lang.org/){ .md-button .md-button--primary }


!!! note

    En utilisant le template de [`arduino-hal`][avr-hal] (via la commande `cargo generate`), il n'est pas nécessaire de se préocupper des versions de Rust.


## crates utilisées

 * [arduino-hal][avr-hal]

## Paquets nécessaires

 * binutils - Pour Debian (et donc Ubuntu)
 * gcc-avr
 * avr-libc
 * avrdude (pour téléverser les programmes sur Arduino)
 * libudev-dev (pour ravedude)
 * pkg-config (pour ravedude)

Assurer vous d'avoir tout les paquets nécessaires d'installés

??? abstract "Installation des paquets"

    === "Debian / Ubuntu"

        ```bash
        sudo apt install binutils gcc-avr avr-lib avrdude libudev-dev pkg-config
        ```

    === "ArchLinux"
        ```bash
        sudo pacman -S avr-gcc avr-libc avrdude systemd pkgconf
        ```

    === "Fedora"
        ```bash
        sudo dnf install avr-gcc avr-libc avrdude systemd-devel pkgconf-pkg-config
        ```

## Initialisation d'un projet

### La bonne version de rustc

Pour commencer, nous avons besoin d'utiliser une version de rustc nightly antérieure à `2021-07-01`. La plus récente que j'ai réussi à utiliser est la version `1.51.0-nightly`.

=== "Installation"

    ```bash
    rustup install nightly-2020-12-30
    ```

=== "Utilisation"

    ```bash
    rustup override set nightly-2020-12-30
    ```

!!! info

    **:calendar: 01/10/2022** : La dernière version pouvant être utilisé est la `nightly-2022-07-10`.

    Pour être sûr d'utiliser la dernière version fonctionnant, vous pouvez tout simplement laisser l'installation 
    se faire au premier lancement d'un programme après avoir générer le projet via `cargo genera` avec le template [`arduino-hal`][avr-hal].

### Installer `ravedude`

???+ info
    
    Si ça n'a pas été fait, il faut installer les dépendances pour [ravedude][ravedude] (voir section [Paquets nécessaires](#paquets-necessaires)).


En utilisant cargo, installez `ravedude` :

```bash
cargo +stable install ravedude
```

### Installer `cargo-generate`

???+ info

    Assurez vous d'avoir le package de développement d'OpenSSL, qui est nécessaire pour `cargo-generate` (voir section [Paquets nécessaires](#paquets-necessaires))

La solution la plus simple pour créer un projet, est de partir d'un template. Si vous souhaitez utiliser cette solution, il est nécessaire d'installer `cargo-generate` :

```bash
cargo install cargo-generate
```

### Créer le projet

#### A partir d'un template

Pour installer le template, on peut utiliser la commande suivante :

```bash
cargo generate --git https://github.com/Rahix/avr-hal-template.git
```

Un nom de projet vous sera demandé, ainsi que le microcontrôlleur que vous voulez utilisé.

Une fois le projet créé, vous pouvez le compiler et le lancer :

```bash
cargo run
```

Cette commande téléverse le programme sur la carte.

??? example

    === "Commande"

        ```bash
        cargo generate --git  https://github.com/Rahix/avr-hal-template.git
        ```

    === "Sortie"

        ```bash
        ⚠️   Unable to load config file: /home/me/.cargo/cargo-generate.toml
        🤷   Project Name : 
        ```

    ===! "Commande"

        Rentrer le nom souhaité :

        ```bash
        ⚠️   Unable to load config file: /home/me/.cargo/cargo-generate.toml
        🤷   Project Name : test
        ```

    === "Sortie"

        ```bash
        ⚠️   Unable to load config file: /home/me/.cargo/cargo-generate.toml
        🤷   Project Name : test
        🔧   Generating template ...
        ? 🤷   Which board do you use? ›
          Arduino Leonardo
          Arduino Mega 2560
          Arduino Nano
        ❯ Arduino Uno
          SparkFun ProMicro
          Nano168
        ```

    ===! "Commande"

        Sélectionner la carte de développement utilisée (utiliser ++up++ ou ++down++ puis ++enter++ pour valider)

    === "Sortie"

        ```bash
        ⚠️   Unable to load config file: /home/me/.cargo/cargo-generate.toml
        🤷   Project Name : test
        🔧   Generating template ...
        ✔ 🤷   Which board do you use? · Arduino Uno
        ...
        🔧   Moving generated files into: `/path/test`...
        ✨   Done! New project created `/path/test
        ```

    ===! "Commande"

        ```bash
        cargo run
        ```

    === "Sortie"

        Pour la première exécution il devrait afficher quelque chose du genre :
        ```bash
        info: syncing channel updates for 'nightly-2021-01-07-x86_64-unknown-linux-gnu'
        info: latest update on 2021-01-07, rust version 1.51.0-nightly (c2de47a9a 2021-01-06)
        info: downloading component 'cargo'
        info: downloading component 'rust-src'
        info: downloading component 'rust-std'
        info: downloading component 'rustc'
        info: installing component 'cargo'
        info: installing component 'rust-src'
        info: installing component 'rust-std'
        info: installing component 'rustc'
        ```

!!! failure

    Si vous obtenez ce message d'erreur, c'est que votre carte de développement n'est pas connectée

    ```bash
    Error: no matching serial port found, use -P or set RAVEDUDE_PORT in your environment

    Caused by: Serial port not found.
    ```

!!! success

    Vous venez de réussir à utiliser Rust avec une carte Arduino :smiley:

    Voici quelques liens utilent qui m'ont permis de découvrir comment faire :

     * [Le livre avr-rust](https://book.avr-rust.com/)
     * [GitHub de avr-hal][avr-hal]

[avr-hal]: https://github.com/Rahix/avr-hal/
[ravedude]: https://github.com/Rahix/avr-hal/tree/main/ravedude
---
title: seven-segments-driver
description: Une [`embeded-hal`][emb-hal] implémentation pour les afficheurs 7-segments multi-digits.
date: 2022-08-23
categories:
  - Projet
  - Embedded
  - Rust
authors:
  - lostpy
tags:
  - rust
  - embedded
---


# 7-segments-driver

[`7-segements-driver`][gitlab] est une [`embeded-hal`][emb-hal] implémentation pour les afficheurs 7-segments multi-digits.  
Cette implémentation permet de facilement utiliser un afficheur 7-segments quelque soit le nombre de digit, il permet également de contrôler les 7-segments à partir d'un shift-register facilement.

[Repository :material-source-repository:][gitlab]{ .md-button }

<!-- more -->

## Exemples

=== "1 digit"

    ```rust title="src/main.rs" linenums="1"
    fn main() -> ! {
        ... // Initialisation of pins and delay (1)
        let seven_seg = SevenSegments::new(a, b, c, d, e, f, g, 1000);  // (2)!
        let mut one_digit_display = OneDigit::new(
            digit,
            seven_seg,
        );

        let mut delay = Delay::new(cp.SYST, &rcc);

        loop {
            for i in 0..9 {
                one_digit_display.display(i, &mut delay);
            }
        }
    }
    ```

    1. Les pins `a`, `b`, `c`, `d`, `e`, `f` et `g` pour les 7 segments ainsi que le pin du `digit`.
    2. 1000 pour afficher un chiffre durant 1000ms

=== "4 digits"

    ```rust title="src/main.rs" linenums="1"
    fn main() -> ! {
        ... // Initialisation of pins and delay (1)
        let seven_seg = SevenSegments::new(a, b, c, d, e, f, g, 7);  // (2)!
        let mut four_digits_display = FourDigits::new(
            digit1,
            digit2,
            digit3,
            digit4,
            seven_seg,
        );

        let mut delay = Delay::new(cp.SYST, &rcc);

        loop {
            for i in 0..1501 {
                four_digits_display.multiplexed_display(i, &mut delay, 200);
            }
        }
    }
    ```

    1. Les pins `a`, `b`, `c`, `d`, `e`, `f` et `g` pour les 7 segments ainsi qu'un pin par digit (`digit1`, `digit2`, `digit3`, `digit4`).
    2. 7 pour afficher chaque digit durant 7ms, une petite durée est utilisée pour un affichage multiplexé.

=== "4 digit & Shift-register"

    ```rust title="src/main.rs" linenums="1"
    fn main() -> ! {
        ... // Initialisation of pins and delay (1)
        let shift_register = ShiftRegister7Seg::new(clock, data, latch, 7); // (2)!
        let mut four_digits_display = FourDigits::new(
            digit1,
            digit2,
            digit3,
            digit4,
            shift_register,
        );

        let mut delay = Delay::new(cp.SYST, &rcc);

        loop {
            for i in 0..1501 {
                four_digits_display.multiplexed_display(i, &mut delay, 200);
            }
        }
    }
    ```

    1. Les pins `clock`, `data`, `latch` pour le shift-register ainsi qu'un pin par digit (`digit1`, `digit2`, `digit3`, `digit4`).
    2. 7 pour afficher chaque digit durant 7ms, une petite durée est utilisée pour un affichage multiplexé.


## Getting Started

Pour utiliser cette crate, il faut l'ajouter dans les dépendances. Ajouter dans les dépendances du fichier `Cargo.toml` :

```
seven-segments-driver = { git = "https://gitlab.com/LostPy/seven-segments-driver.git" branch = "master" }
```

Pour rapidement tester cette implémentation, vous pouvez également clone le dếpôt git et exécuter les exemples.

!!! warning

    Les exemples sont disponibles pour un nombre limité de plateformes. Si vous avez une autre plateforme supportée par [`embedded-hal`][emb-hal], le code devra être partiellement réécris.

???+ example

     1. Clone git repo:
    ```
    git clone https://gitlab.com/LostPy/seven-segments-driver.git
    ```

     2. move in an example directory

    ```
    cd examples/blocking/sevensegments/stm32f0xx
    ```

     3. Run and Flash

    ```
    cargo run --release
    ```

## Documentation

Vous pouvez voir la documentation en utilisant la commande `cargo doc`.

??? example "Commande"

    ```
    cargo doc --package seven-segments-driver --open
    ```

## Async API

L'API assynchrone n'est pas encore disponible.

## Caractéristiques

??? info
    
    Les caractéristiques non cochées ne sont pas encore disponible mais sont planifiées.

 - [x] Blocking API
    - [x] Trait for single 7-segments
    - [x] Trait to build custom multi digits 7-segments
    - [x] Support for a single digit 7-segments
    - [x] Support for 4 digits 7-segments
    - [x] Support for 7-segments control by shift-register
 - [ ] Async API 
    - [ ] Trait for single 7-segments
    - [ ] Trait to build custom multi digits 7-segments
    - [ ] Support for a single digit 7-segments
    - [ ] Support for 4 digits 7-segments

[gitlab]: https://gitlab.com/LostPy/seven-segments-driver
[emb-hal]: https://github.com/rust-embedded/embedded-hal
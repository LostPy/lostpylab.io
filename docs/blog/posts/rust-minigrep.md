---
title: minigrep
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-08-08
categories:
  - Projet
  - Rust
authors:
  - lostpy
tags:
  - rust
  - cli
  - learning
---

# rust-minigrep

!!! info

    Cette application a été faite dans le cadre du livre [The Rust Programming Language](https://doc.rust-lang.org/book/ch12-00-an-io-project.html).

`rust-minigrep` est un programme similaire à `grep` (globally search a regular expression and print), qui recherche un groupe de caractères dans un fichier et renvoie les lignes du fichier qui possèdent le groupe de caractères.

[Repository :material-source-repository:](https://github.com/lostpy/rust-minigrep){ .md-button }

<!-- more -->

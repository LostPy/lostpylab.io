---
title: osu-api
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2021-01-11
categories:
  - Projet
  - Game
  - Python
authors:
  - lostpy
tags:
  - python
  - python package
  - library
  - rest api
  - osu!
---

# osu-api

`osu-api` est un package Python pour utiliser l'API (V1) du jeu [osu!](https://osu.ppy.sh).

[Repository :material-source-repository:](https://github.com/lostpy/osu-api.py){ .md-button }

<!-- more -->

---
title: Rust Ant Simulator
description: Ce site est le lieux où je partage mes projets et mes articles.
date: 2022-08-15
categories:
  - Projet
  - Simulation
  - Rust
authors:
  - lostpy
tags:
  - rust
  - science
  - simulation
---


# RustAco

[RustAco][gitlab] est une simulation de colonie de fourmie inspirer de mon précédent projet [PyGAco][pygaco]. 
Elle est plus configurable et par rapport à [PyGAco][pygaco], elle implémente un algorithme qui permet aux fourmies "perdues" de
retrouver leur chemin vers la colonie. Cette version permet de simuler sans lag jusqu'à 3000 fourmies.

[Repository :material-source-repository:][gitlab]{ .md-button }

<!-- more -->

![type:video](../../assets/videos/rustaco.mp4)

## Utilisation

### Requirements

 * Rust

??? note "Instructions"

    Il suffit de cloner le dépôt git et d'avoir Rust installé pour le tester :

    ```
    git clone https://gitlab.com/LostPy/rust-ant-simulator.git
    ```

    ```
    cd rust-ant-simulator/
    ```

Pour lancer une simulation :

```
cargo run --release
```

## Configuration

Il est possible de modifier les paramètres dans `rustaco.toml`.

Au démarrage de la simulation, s'il n'y a pas de fichier de configuration `rustaco.toml`, le fichier est créé avec les valeurs par défaut. 
Vous pouvez modifier ce fichier, pour mettre à jour il suffit de redémarrer la simulation avec la touche ++esc++.

### Détails

!!! info

    Tout les champs sont optionnel.

!!! warning

    La fonctionnalité des map n'est pas encore implémenté (la config `map` est ignorée).

|Field name|Type|Description|Default|
|:--------:|:--:|-----------|:-----:|
|`application`|`object`|L'ensemble des paramètre lié à l'application (fenêtre...).||
|`application.window-width`|`u16`|La largeur de la fenêtre, ignoré si une map est utilisée.|`800`|
|`application.window-height`|`u16`|La hauteur de la fenêtre, ignoré si une map est utilisée.|`600`|
|`map`|`object`|L'ensemble des paramètre lié à la map.||
|`map.type`|`"none"`, `"generated"` ou `"tiled"`|Type de map: `none` pour "no map", `generated` pour générer une map au démarrage (unimplemented) and `tiled` pour utiliser une Tiled Map (fichier tmx) (unimplemented)|`"none"`|
|`map.generator`|`object`|Paramètres pour le générateur de map.||
|`map.generator.tile-size`|`u8`|Taille d'une tuile en pixel (une tuile est un carré).|`16`|
|`map.generator.width`|`u8`|Nombre de tuiles dans la largeur.|`50`|
|`map.generator.height`|`u8`|Nombre de tuiles dans la hauteur.|`31`|
|`map.generator.init-fill-rate`|`f32`|Le taux de remplissage initial.|`0.7`|
|`map.generator.number-iterations`|`u8`|Le nombre d'itération de lissage.|`2`|
|`simulation`|`object`|Paramètres lié à la simulation.||
|`simulation.time-update-pheromone`|`f32`|Temps entre 2 mise à jour des phéromones (Peut être utilisé pour améliorer la simulation, mais diminue les performance -> + RAM consommé)|Dépendant du nombre de fourmies|
|`simulation.ant-count`|`u16`|Nombre de fourmies, peut aller jusqu'à 3000|`30`|
|`simulation.ant`|`object`|Paramètres liés aux fourmies.||
|`simulation.ant.speed`|`u8`|Vitesse des fourmies (pixel / seconde).|`120`|
|`simulation.ant.angle-view`|`u16`|Angle du champs de vision des fourmies (en degrés).|`90`|
|`simulation.ant.distance-view`|`u8`|Distance de vue des fourmies (en pixel).|`150`|

### Exemple

```toml
[application]
window-width = 950
window-height = 700

[simulation]
ant-count = 200

[simulation.ant]
speed = 100
distance-view = 80
```


[gitlab]: https://gitlab.com/LostPy/rust-ant-simulator
[pygaco]: https://gitlab.com/LostPy/pygaco



/*
* Update when change tab (for mermaid in tab)
*/

function updateTabMermaid(event) {
  const tab = event.target;
  const current = document.querySelector(`label[for=${tab.id}]`);
  if (current.textContent == "Résultat") {
    location.reload();
  }
}

const blocsMermaid = document.getElementsByClassName("mermaid-in-bloc");
for (const bloc of blocsMermaid) {
  let tabsMermaid = bloc.querySelectorAll(".tabbed-set > input");
  for (let tab of tabsMermaid) {
    tab.addEventListener("click", updateTabMermaid);
  }
}

/*
* Update theme 
*/

let paletteSwitcher1 = document.getElementById("__palette_1");
let paletteSwitcher2 = document.getElementById("__palette_2");

paletteSwitcher1.addEventListener("change", function () {
  location.reload();
});

paletteSwitcher2.addEventListener("change", function () {
  location.reload();
});

---
title: LostPy's Planet
description: Ce site est le lieux où je partage mes projets et quelques articles.
hide:
  - navigation
  - toc
---


![lostpylogo](assets/logos/LostPyLogo.gif){ loading=lazy }

# Welcome to LostPy's website

Ce site est le lieu où je partage mes projets. Il s'agit principalement de package Python et d'interface graphique... Pour certains des projets, le seul objectif est l'apprentissage.


## Social

 * [GitLab :fontawesome-brands-gitlab:](https://www.gitlab.com/lostpy)

 * [GitHub :fontawesome-brands-github:](https://www.github.com/lostpy)

